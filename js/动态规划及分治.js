/**
 * 给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。子数组 是数组中的一个连续部分。
 * 示例 1：
    输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
    输出：6
    解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。

    示例 2：
    输入：nums = [1]
    输出：1
 * **/ 


/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    let max = 0;
    let pre = nums[0]

    for(let i = 0; i < nums.length; i++){
        pre = Math.max(pre + nums[i], nums[i])

        max = Math.max(pre, max)
    }

    return max
}

// console.log(maxSubArray([-2,1,-3,4,-1,2,1,-5,4]))

/**
 * @description 背包问题：包的容量为4，计算出重量不超过背包容器的容量下的物品最大价值
 * @param {number[]} nums
 * @return {number}
 */
let arr = [
    {
        w: 1,
        v: 1500
    },
    {
        w: 4,
        v: 3000
    },
    {
        w: 3,
        v: 2000
    }
]

// todo: 待实现
let maxSubArray2 = function(nums, maxW = 4) {
    let totalW = 0
    let v = 0
    nums.forEach(item=>{
        
    })
}

