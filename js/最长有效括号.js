/**
 给你一个只包含 '(' 和 ')' 的字符串，找出最长有效（格式正确且连续）括号子串的长度。
    示例 1：
    输入：s = "(()"
    输出：2
    解释：最长有效括号子串是 "()"


    示例 2：

    输入：s = ")()())"
    输出：4
    解释：最长有效括号子串是 "()()"
 * **/

/**
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function(s) {
    if(s.length <= 1) return 0

    let arr = new Array(s.length).fill(0)

    let stack = []

    for(let i = 0; i <= s.length - 1; i++){
        if(s[i] === '('){
            stack.push(i)

            continue
        }

        if(stack.length > 0){
            arr[i] = 1
            arr[stack.pop()] = 1
        }

    }

    let max = 0

    let count = 0

    arr.forEach(item=>{
        if(item === 1){
            count++
        }else{
            count = 0
        }

        max = max > count ? max : count
    })

    return max
}

console.log(longestValidParentheses(')(()()))'))