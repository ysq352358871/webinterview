/**
 * 1. 给定两个数组 nums1 和 nums2 ，返回 它们的交集 。输出结果中的每个元素一定是 唯一 的。我们可以 不考虑输出结果的顺序 。
 *  **/


let num1 = [1,2,2,1]
let num2 = [2,2]

function getIntersection(arg1, arg2){
    if(!(arg1 instanceof Array) || (!arg2 instanceof Array)){
        console.error('数据有误！')
        return false
    }
    let intersectionArr = []
    
    let arr = arg1.length > arg2.length ? arg1 : arg2

    arr.forEach(item=>{
        if(arg1.includes(item) && arg2.includes(item) && !intersectionArr.includes(item)){
            intersectionArr.push(item)
        }
    })

    return intersectionArr
}

function getIntersection2(arg1, arg2){
    if(!(arg1 instanceof Array) || (!arg2 instanceof Array)){
        console.error('数据有误！')
        return false
    }

    let result = new Set()

    if(arg1.length < arg2.length){
        let _ = arg1

        arg1 = arg2

        arg2 = _
    }

    let orgData = new Set(arg1)

    for(let i = 0; i < arg2.length; i++){
        orgData.has(arg2[i]) && result.add(arg2[i])
    }

    return Array.from(result)
}